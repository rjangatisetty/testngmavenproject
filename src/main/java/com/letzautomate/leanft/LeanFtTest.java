package com.letzautomate.leanft;

import com.hp.lft.report.Reporter;
import com.hp.lft.sdk.stdwin.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import com.hp.lft.sdk.*;
import com.hp.lft.verifications.*;
import repositories.Notepad;


public class LeanFtTest {

        @BeforeMethod
        public void beforeMethod() {
            try {

                ModifiableSDKConfiguration config = new ModifiableSDKConfiguration();
                config =  SDKConfigurationFactory.loadConfigurationFromExternalPropertiesFile("src\\main\\resources\\leanft.properties");
                //config.setServerAddress(new URI("ws://localhost:5095"));
                SDK.init(config);
                Reporter.init();
            }catch (Exception ex){
                System.out.println("Exception occured " + ex.toString());
            }
        }

        @AfterMethod
        public void afterMethod() throws Exception {
            Reporter.generateReport();
            SDK.cleanup();
        }

        @Test
        public  void testNotepadExample() throws Exception{

            new ProcessBuilder("C:\\Windows\\System32\\notepad.exe").start();
            Thread.sleep(3000);
            Notepad notepad = new Notepad();
            notepad.notepadWindow().editEditor().sendKeys("This is Notepad");
            Thread.sleep(3000);

        }
    }




